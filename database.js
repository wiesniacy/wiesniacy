export {game, player } ;
var player = {
  name : '',
  gold: 0,
  strenght : 1,
  agility : 1,
  wit : 1,
  skill_points: 3,
  perks : {
    perk1 : false,
    perk2 : false,
    perk3 : false,
    perk4 : false,
    perk5 : false
  },
  changeStat: function(stat_name, value){
    switch(stat_name){
      case  'strenght': this.strenght = value;
            break;
      case  'agility': this.agility = value;
            break;
      case  'wit': this.wit = value;
            break;
      default: console.log("Incorrect value/ stat_name.");
    };
  },
  modifyPlayer: function(name,strenght,agility,wit){
    this.name = name.toString();
    this.strenght = Number(strenght);
    this.agility = Number(agility);
    this.wit = Number(wit);
  }
};

var game = {
  players: 0,
  global_gold: 0,
  amount_of_items: 0,
  configure: function(players,global_gold, amount_of_items){
    this.players = players;
    this.global_gold = global_gold;
    this.amount_of_items = amount_of_items;
  }
}