import { game } from './database.js';

var numOfPlayers = document.querySelector('#numOfPlayers');
var amountOfTime = document.querySelector('#amountOfTime');

export function get_game_settings(){
  console.log(`Number of players: ${numOfPlayers.value}, amount of time for the game: ${amountOfTime.value}`);
}

let new_game = Object.create(game);
export function initializeGame(){
  new_game.configure(numOfPlayers.value, Math.floor(numOfPlayers.value*1.2), 10); // players | global_gold | amount_of_items
  console.log(`Game created! Players:${new_game.players}, gold:${new_game.global_gold}, items:${new_game.amount_of_items}`);
};